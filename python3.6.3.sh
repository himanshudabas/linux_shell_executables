sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get install build-essential libssl-dev libffi-dev -y
wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tgz
tar xfz Python-3.6.3.tgz
cd Python-3.6.3/
./configure --prefix /usr/local/lib/python3.6
sudo make
sudo make install
sudo rm -rf /usr/bin/python /usr/bin/python3.6 /usr/bin/pip
sudo ln -s /usr/local/lib/python3.6/bin/python3.6 /usr/bin/python
echo "linked python"
sudo ln -s /usr/local/lib/python3.6/bin/python3.6 /usr/bin/python3.6
echo "linked python3.6"
sudo ln -s /usr/local/lib/python3.6/bin/pip3 /usr/bin/pip
echo "linked pip3"
