cd ~/Downloads/
wget https://nodejs.org/dist/v9.1.0/node-v9.1.0-linux-x64.tar.xz
sudo tar -xf ./node-v9.1.0-linux-x64.tar.xz -C /usr/local/lib
cd /usr/local/lib
sudo mv ./node-v9.1.0-linux-x64 ./nodejs
sudo rm -rf /usr/bin/node /usr/bin/nodejs /usr/bin/npm /usr/bin/npx
sudo ln -s /usr/local/lib/nodejs/bin/node /usr/bin/node
sudo ln -s /usr/local/lib/nodejs/bin/node /usr/bin/nodejs
sudo ln -s /usr/local/lib/nodejs/bin/npm /usr/bin/npm
sudo ln -s /usr/local/lib/nodejs/bin/npx /usr/bin/npxc