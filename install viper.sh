cd ~
virtualenv -p python --no-site-packages ~/viper-venv
source ~/viper-venv/bin/activate
sudo apt-get install git -y
git clone https://github.com/ethereum/viper.git
cd viper
sudo apt-get install libgmp-dev
sudo apt-get install pkg-config
make
echo "SUCESSFULLY INSTALLED VIPER"