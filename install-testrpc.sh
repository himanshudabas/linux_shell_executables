sudo npm install -g ethereumjs-testrpc
cd ~
touch ~/.bash_aliases
echo 'alias testrpc="node /usr/local/lib/nodejs/lib/node_modules/ethereumjs-testrpc/build/cli.node.js"' >> ~/.bash_aliases
echo " Successfully installed TestRPC. Start a chain by entering "testrpc" in the terminal"
